import React, { Component } from 'react'

export default class Stepper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentStep: -1,
      refresh: true,
      showCardBg: true,
      ...props
    };
  }

  componentWillReceiveProps(nextProps) {
    const vm = this
    const body = document.documentElement
    body.scrollTop = 0
    vm.setState({ 
      // refresh: false,
      currentStep: nextProps.currentStep
    })
    setTimeout(() => {
      vm.setState({ 
        // refresh: true
      })
    }, 300)
  }

  render () {
    const { currentStep, refresh, showCardBg } = this.state
    const { children, startPage, endPage, extraModule, nextStep } = this.props
    const totalSteps = children.length - 1

    return (
      <div className={`stepper transition ${ refresh ? 'updated' : ''}`}>
        <div className="grid-x align-middle align-center-middle">
          <div className="cell small-12 medium-12 large-8 text-align-center">
            <div className={`${ showCardBg? 'card white' : '' } grid-x align-center-middle steps`}>
              <div className="cell small-12">
                <div className="grid-x align-justify steps-header">
                  <div className={`progress`}>
                    <div className="progress-meter" style={{ width: `${(( currentStep > totalSteps ? totalSteps :  currentStep ) / totalSteps) * 100}%` }}/>
                  </div>
                  {
                    (children || []).map((child, index) => {
                      if (currentStep > index || currentStep === totalSteps) {
                        return (
                          <div className="cell auto location completed" key={index} onClick={(e) => nextStep(e, index)}>
                            <h3>{child.props && child.props.name ? child.props.name : `Step ${index + 1}`}</h3>
                            <div className={`circle`} />
                          </div>
                        )
                      } else {
                        return (
                          <div className={`cell auto location ${currentStep === index ? 'active' : ''}`} key={index}>
                            <h3>{child.props && child.props.name ? child.props.name : `Step ${index + 1}`}</h3>
                            <div className={`circle`} />
                          </div>
                        )
                      }
                    })
                  }
                </div>
              </div>
            </div>

            <div className={`${ showCardBg? 'card white' : '' } grid-x align-center`}>
              <div className={`stepper-child cell small-12 ${(currentStep === -1) ? 'right-fade active' : 'hide left-fade-out'}`}> { startPage }</div>
              {
                (children || []).map((child, index) => {
                  return (
                    <div className={`stepper-child cell small-12 ${(currentStep === index) ? 'right-fade active' : 'hide left-fade-out'}`} key={index}>{child}</div>
                  )
                })
              }
              <div className={`stepper-child cell small-12 ${(currentStep > totalSteps) ? 'right-fade active' : 'hide left-fade-out'}`}> { endPage }</div>
              { extraModule }
            </div>
          </div>
        </div>
      </div>
    )
  }
}