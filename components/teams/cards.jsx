import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import Modal from '../partials/modal'
import AddCard from '../teams/add-card'
import Loader from '../partials/loader'

export default class TeamCards extends Component {
  constructor(props) {
    super(props)

    this.state = {
      amount: null,
      showModal: false,
      debit_card: null,
      showLoading: false,
      inputStates: {},
      ...props
    }

    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.onBlur = this.onBlur.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ activeGoal: nextProps.activeGoal })
  }

  openModal() {
    this.setState({ showModal: true })
  }

  closeModal (e) {
    e.preventDefault()
    this.setState({ showModal: false })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState((state) => {
      let updatedState = state
      updatedState[name] = value
      return updatedState
    })
  }

  onFocus(event) {
    const target = event.target
    const name = target.name

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = true
      return updatedState
    })
  }

  onBlur(event) {
    const target = event.target
    const name = target.name

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = false
      return updatedState
    })
  }


  render () {
    const { item } = this.props
    const { showModal, debit_card } = this.state
    
    const Participants = item.participants ? (<p>Participants: { item.participants } </p> ): null
    const frequency = (
      <div className="cell small-4 text-align-right">
        {  Participants }
        <p>
          Frequency: { item.frequency }
        </p>
      </div>
    )
  
    return (
      <div>
        <div className={`card goals ${this.state.activeGoal === item.id ? 'alternate' : 'white' }`}>
          <div className="grid-x" style={{paddingBottom: '15px'}}>
            <div className="cell small-8">
              <h3>{ item.title }</h3>
            </div>
            <div className="cell small-4 text-align-right">
              <p className="interest-returns">+ <small>₦</small>{ item.interest_returns || 0 }</p>
              <p><small>(Interest Earned)</small></p>
            </div>
          </div>

          <br/>

          <div className="grid-x" style={{paddingBottom: '15px'}}>
            <div className="cell small-8">
              <p className="interest-returns"><small>₦</small>{ item.amount_saved || 0}</p>
            </div>
            <div className="cell small-4 text-align-right">
              <p className="interest-returns"><small>₦</small>{ item.total_amount || 0 }</p>
            </div>
            <div className="cell small-12">
              <div className={`${item.progress_type} progress`}>
                <div className="progress-meter" style={{width: item.progress }}></div>
              </div>
            </div>
            <div className="cell small-8">
              <p>Amount Saved</p>
            </div>
            <div className="cell small-4 text-align-right">
              <p>Goal</p>
            </div>
          </div>

          <br/>
          <div className="overlay" onClick={(e) => this.props.setActiveGoal(e, item)}/>
          <div className="grid-x align-middle">
            <div className="cell small-7 medium-8">
              <p>Next Saving Date: { item.saving_date }</p>
            </div>
            <div className="cell small-5 medium-4 text-align-right">
              <button className="button purple expanded no-margin add-fund"
                onClick={this.openModal}
                >
                BOOST SAVINGS
              </button>
            </div>
          </div>
        </div>
        <Modal showModal={ this.state.showModal } closeModal={this.closeModal}>
          <a className="close" onClick={(e) => this.closeModal(e)}/>
          <form name="add-funds">
            <div className="grid-x align-middle align-center-middle">
              <div className="cell small-12">
                <div className="grid-x grid-padding-x">
                  <div className="cell small-12 input-selector">
                    <h2 className="text-center">Add Funds</h2>
                    <p className="summary">
                      Reach your set target as you save consistently and earn new badges
                    </p>
                    <br/>
                    <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.amount ? 'active' : ''}`}>Enter Amount</label>
                    <input type="number" 
                      min="0" name="amount"
                      onFocus={this.onFocus}
                      onBlur={this.onBlur}/>
                  </div>
                  <div className="cell small-12">
                    <div className="input-selector">
                      <label className="text-align-left">Select from your list of debit cards</label>
                      <select defaultValue={item.last_digits} name="debit_card" onChange={this.handleInputChange}>
                        <option>Select Debit Card</option>
                        <option value="add-new-card">Add New Debit Card</option>
                        {
                          (this.props.cardLists || []).map((card, index) => {
                            return (
                              <option value={card.last_digits} key={index}>{card.bank_name} **** ****{card.last_digits}</option>
                            )
                          })
                        }
                      </select>
                    </div>
                    { 
                      (debit_card === "add-new-card")? (
                        <div className="cell small-12">
                          <AddCard  
                            {...this}
                            handleInputChange={this.handleInputChange}
                            />
                        </div>
                      ) : null
                    }
                  </div>
                  <div className="cell small-12">
                    <button className="button purple expanded max-340" onClick={(e) => {
                      e.preventDefault()
                      const vm = this
                      vm.setState({
                        showLoading: true
                      })
                      setTimeout(() => {
                        vm.closeModal(e)
                        vm.setState({
                          showLoading: false
                        })
                      }, 1000)
                    }}>
                      SAVE
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          {
            this.state.showLoading ? (<Loader showLoading={this.state.showLoading} size={100}/>) : null
          }
        </Modal>
      </div>
    )
  }
}