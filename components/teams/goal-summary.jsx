
import React, { Component } from 'react'
import Link from 'next/link'
import GoalContributions from './contributors'
import {AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts'
import Loader from '../partials/loader'

class GoalSummary extends Component {
  constructor (props) {
    super(props);
    this.state = {
      goal: {},
      refresh: true,
      showLoading: false,
      ...props
    };
  }

  componentWillReceiveProps(nextProps) {
    const vm = this
    vm.setState({ 
      refresh: nextProps.refresh,
      showLoading: nextProps.showLoading,
      goal: nextProps.goal 
    })
  }

  render () {
    const { goal, refresh, showLoading } = this.state 

    return (
      <div className={`card white overview-dashboard transition ${ refresh ? 'updated' : ''}`}>
        <div className="grid-x grid-padding-x align-top">
          <div className="cell medium-12 large-6 goal-summary">
            <div className="grid-x grid-padding-x">
              <div className="cell small-7 medium-6">
                <h4>
                  GOAL SUMMARY
                </h4>
              </div>
              <div className="cell small-5 medium-6 text-align-right">
                <Link prefetch href={{ pathname: '/account/savings/my-goals' }}>
                  <a className="purple-text manage-goal">
                    <span className="icon">
                      <img src={require('../../assets/img/settings-icon.png')} width="15"/>
                    </span>
                    Manage Goal
                  </a>
                </Link>
              </div>
            </div>
            
            <div className="grid-x grid-padding-x">
              <div className="cell small-12 right-fade" style={{height: '350px'}}>
                <ResponsiveContainer>
                  <AreaChart data={goal.stats}>
                    <XAxis dataKey="name"/>
                    <YAxis/>
                    <Area type='monotone' dataKey='uv' stroke='#8000fa' fill='#8000fa' />
                  </AreaChart>
                </ResponsiveContainer>
              </div>
            </div>
            <hr/>
            <div className="grid-x grid-padding-x">
              <div className="cell small-12 medium-6 savings">
                <h4>SAVING FREQUENCY</h4>
                <br/>
                <div className="grid-x grid-padding-x align-middle right-fade">
                  <div className="cell small-9 medium-6">
                    <p className="funds small"><small>₦</small>{ goal.frequency_returns || 0 }</p>
                    <p className="text">
                      <strong>{ goal.frequency || '---' } Savings</strong><br/>
                      <span className="date">Start Date: { goal.saving_date || '---' }</span>
                    </p>
                  </div>
                  <div className="cell small-3 medium-6">
                    <img src={require('../../assets/img/calendar-icon.png')} width="60"/>
                  </div>
                </div>
              </div>
              <div className="cell small-12 medium-6 divider debit-card-assigned">
                <h4>DEBIT CARD ASSIGNED</h4>
                <br/>                  
                <div className="grid-x grid-padding-x current-card left-fade align-middle">
                  <div className="cell small-9 medium-7">
                    <p className="text masked-card">
                      <strong>**** **** **** {goal.last_digits || '****'}</strong>
                    </p>
                    <p>
                      <img src={require('../../assets/img/visa-icon.png')} width="52"/>
                    </p>
                  </div>
                  <div className="cell small-3 medium-5">
                    <img src={require('../../assets/img/my-card-icon.png')} width="60"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="cell medium-12 large-6 divider contributions">
            <div className="grid-x grid-padding-x">
              <div className="cell small-6 left-fade">
                <h3 className="funds purple-text"><small>₦</small>{ goal.total_amount || 0 }</h3>
                <h4>GOAL BALANCE</h4>
              </div>
              <div className="cell small-6 right-fade text-align-right">
                <p className="funds small"><small>₦</small>{ goal.amount_saved || 0 }</p>
                <h4><small>AMOUNT SAVED</small></h4>
                <p className="funds small"><small>₦</small>{ goal.interest_returns || 0 }</p>
                <h4><small>INTEREST EARNED</small></h4>
              </div>
            </div>
            <div className="grid-x grid-padding-x">
              <div className="cell small-6">
                <h4>RECENT CONTRIBUTIONS</h4>
              </div>
              <div className="cell small-6 text-align-right"></div>
            </div>
            {
              (goal.contributions || []).map((item, index) => {
                return (
                  <GoalContributions className="up-fade" item={item} key={index}/>
                )
              })
            }
          </div>
        </div>
        {
          showLoading ? (<Loader showLoading={showLoading} size={100}/>) : null
        }
      </div>
    )
  }
}

export default GoalSummary