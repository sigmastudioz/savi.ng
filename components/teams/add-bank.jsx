
import React, { Component } from 'react'

export default class AddBank extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputStates: {},
        ...props
      }
  
      this.onFocus = this.onFocus.bind(this)
      this.onBlur = this.onBlur.bind(this)
    }
  
    onFocus(event) {
      const target = event.target
      const name = target.name
  
      this.setState((state) => {
        let updatedState = state
        updatedState.inputStates[name] = true
        return updatedState;
      })
    }
  
    onBlur(event) {
      const target = event.target;
      const name = target.name;
  
      this.setState((state) => {
        let updatedState = state
        updatedState.inputStates[name] = false
        return updatedState;
      })
    }

  render () {
    return (
      <div className="grid-x grid-padding-x">
        <div className="cell small-12 input-selector">
          <h2>Add Bank</h2>
          <p>Enter your Bank Account Details</p>
          <label  className={`text-align-left ${this.state.inputStates && this.state.inputStates.account_number ? 'active' : ''}`}>Add Bank Account Number</label>
          <input type="text" 
            name="account_number" 
            placeholder="All Digits"
            onChange={this.props.handleInputChange}
            onFocus={this.onFocus} 
            onBlur={this.onBlur}/>
        </div>
        <div className="cell small-12 input-selector">
          <label  className={`text-align-left ${this.state.inputStates && this.state.inputStates.bank_name ? 'active' : ''}`}>Bank Name</label>
          <input type="text" 
            name="bank_name"
            onChange={this.props.handleInputChange}
            onFocus={this.onFocus} 
            onBlur={this.onBlur}/>
        </div>
        <div className="cell small-12 input-selector">
          <label  className={`text-align-left ${this.state.inputStates && this.state.inputStates.routing_number ? 'active' : ''}`}>Routing Number</label>
          <input type="text" 
            name="routing_number"
            onChange={this.props.handleInputChange}
            onFocus={this.onFocus} 
            onBlur={this.onBlur}/>
        </div>
        <div className="cell small-12">
          <button className="button purple expanded max-340" onClick={(e) => this.props.callBack(e)}>
            SAVE
          </button>
        </div>
      </div>
    )
  }
}