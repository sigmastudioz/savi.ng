
import React, { Component } from 'react'

export default class AddCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showTitle: true,
      showSave: true,
      inputStates: {},
      ...props
    };

    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onFocus(event) {
    const target = event.target;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = true
      return updatedState;
    })
  }

  onBlur(event) {
    const target = event.target;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = false
      return updatedState;
    })
  }

  render () {
    return (
      <div className="grid-x grid-padding-x">
        <div className="cell small-12">
          {this.state.title}
          {this.state.description}
        </div>
        <div className="cell small-12 medium-6 input-selector">
          <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.card_number ? 'active' : ''}`}>Enter Card Number</label>
          <input 
            type="text" 
            name="card_number" 
            placeholder="All Digits"
            onChange={this.props.handleInputChange}
            onFocus={this.onFocus} 
            onBlur={this.onBlur}/>
        </div>
        <div className="cell small-12 medium-6 input-selector">
          <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.cvv ? 'active' : ''}`}>CVV Number</label>
          <input 
            type="text" 
            name="cvv" 
            placeholder="000" 
            maxLength="4"
            onChange={this.props.handleInputChange}
            onFocus={this.onFocus} 
            onBlur={this.onBlur}/>
        </div>
        <div className="cell small-12 input-selector">
          <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.expire_date ? 'active' : ''}`}>Expiry Date</label>
          <input 
            type="date" 
            name="expire_date" 
            placeholder="MM/YY"
            onChange={this.props.handleInputChange}
            onFocus={this.onFocus} 
            onBlur={this.onBlur}/>
        </div>
        {this.state.button}
      </div>
    )
  }
}