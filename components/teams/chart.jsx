import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import Modal from '../partials/modal';

export default class Chart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      labels: null,
      stats: null,
      ...props
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ activeGoal: nextProps.activeGoal })
  }

  openModal() {
    this.setState({ showModal: true });
  }

  closeModal (e) {
    e.preventDefault()
    this.setState({ showModal: false });
  }


  render () {
    const { labels = [], stats = {} } = this.state;
    const counts = labels.map(choice => stats[choice] || 0);
    const totalCount = counts.reduce((total, count) => total + count, 0);

    const chartData = {
      labels: labels,
      datasets: [
        {
          lineTension: 0,
          backgroundColor: 'rgba(68, 204, 153, 0.05)',
          borderColor: 'rgba(68, 204, 153, 0.9)',
          borderWidth: 2,
          borderJoinStyle: 'round',
          pointRadius: 5,
          pointBorderColor: '#fff',
          pointBackgroundColor: 'rgba(68, 204, 153, 0.9)',
          pointBorderWidth: 3,
          data: counts
        }
      ]
    };

    const chartOptions = {
      layout: { padding: { top: 25, bottom: 75, left: 75, right: 75 } },
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: { beginAtZero: true, display: false }
        }]
      },
      legend: { display: false },
      title: {
        display: true,
        text: 'POLL COUNTS',
        padding: 10,
        lineHeight: 4,
        fontSize: 20,
        fontColor: '#677'
      }
    }

  
    return (
      <div className="chart">
        
      </div>
    )
  }
}