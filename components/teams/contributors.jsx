import React, { Component } from 'react'

export default class TeamCards extends Component {
  render () {
    const { item, className } = this.props
    let img = null
    if (item.imageUrl) {
      img = <img src={item.imageUrl} />
    } else {
      img = <p>{item.firstName.substring(0, 1)}</p>
    }

    return (
      <div className={`grid-x contribution align-middle ${className}`}>
        <div className="cell small-12 medium-6">
          <div className="grid-x align-middle">
            <div className="cell auto">
              <div className="avatar align-middle align-center">{img}</div>
            </div>
            <div className="cell small-8">
              <p>{ item.firstName } { item.lastName }</p>
            </div>
          </div>
        </div>
        <div className="cell small-12 medium-2 text-align-center text-small-align-left">
          <p>{ item.saving_date }</p>
        </div>
        <div className="cell small-12 medium-4 text-align-right amount text-small-align-left">
          <p className={`${item.type}`}><small>₦</small>{ item.contribution }</p>
        </div>
      </div>
    )
  }
}