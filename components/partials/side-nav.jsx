import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'
import Router from 'next/router'

class Header extends Component {
  render () {
    const { url } = this.props
    const menus = [
      {
        title: "My Saving",
        className: "saving-icon",
        url: "my-saving"
      },
      {
        title: "Team Savings",
        className: "team-icon",
        url: "team-saving"
      },
      {
        title: "Badges",
        className: "badges-icon",
        url: "badges"
      },
      {
        title: "My Banks",
        className: "my-banks-icon",
        url: "my-banks"
      },
      {
        title: "My Cards",
        className: "my-cards-icon",
        url: "my-cards"
      },
      {
        title: "Invites",
        className: "invites-icon",
        url: "invites"
      }
    ]
    return (
      <div className="side-nav grid-y align-middle">
        <div className="cell nav-home">
          <Link route={'home'}><a className="home-icon"></a></Link>
        </div>
        <div className="cell auto">
          <ul className="menu-list">
            {
              menus.map((menu, index) => {
                return (
                  <li key={index}>
                    <Link route={menu.url}><a className={menu.className}>{menu.title}</a></Link>
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    )
  }
}

export default withRouter(Header)