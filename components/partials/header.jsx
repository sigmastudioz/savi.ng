import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Menu from './menu'


class Header extends Component {
  render () {
    const { children, router, href, openMenu, menuStatus } = this.props
    return (
      <header>
        <div className="grid-x header align-middle" style={{padding : '15px 75px'}}>
          <div className="cell small-2 show-for-small-only mobile-menu">
            <a className="toggle-menu" onClick={(e) => openMenu(e)}>
              
              {
                menuStatus? (
                  <FontAwesomeIcon icon="times"/>
                ) : (
                  <FontAwesomeIcon icon="bars"/>
                )
              }
            </a>
          </div>

          <div className="cell small-3 text-align-left">
            <Link prefetch href={{ pathname: '/account/savings' }}>
              <a>
                <img src={require('../../assets/img/saviLogo.svg')} style={{height : '50px'}}/>
                <span className="badge badge-pill badge-danger">Beta</span>
              </a>
            </Link>
          </div>
          <div className="cell auto show-for-medium">
            <h3 className="text-align-center funds-update" style={{fontSize: '24px'}}>
              Total saved  this year <span className="funds-stat"><small>₦</small>42,000.00</span>
            </h3>
          </div>
          {/* <div className="cell auto">
            <form className="search align-middle">
              <input placeholder="Search..." type="text" name="search"/>
            </form>
          </div> */}
          <div className="cell small-2 text-align-right user-profile show-for-medium">
            <div className="avatar align-middle align-center">
              <img src={require('../../assets/img/pp.jpg')}/>
            </div>          
          </div>
        </div>
        <div className="grid-x align-center-middle site-wrapper show-for-medium">
          <div className="cell small-10 medium-8 large-6">
            <Menu/>
          </div>
        </div>
      </header>
    )
  }
}

export default withRouter(Header)