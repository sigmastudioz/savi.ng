


import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'

class Footer extends Component {
  render () {
    return (
      <footer className="footer">
        <div className="footer-content">
          <div className="grid-x">
            <div className="cell small-12 medium-7">
              <p>
                5th Floor, Elephant House, 214 Broad Street,<br/>
                Marina, Lagos-Island, Nigeria. <br/>
                <a href="mailto:hello@vfd-mfb.com">hello@vfd-mfb.com</a>   |   <a href="tel:+23208189989898">0818 998 9898</a>
              </p>
              <div className="grid-x">
                <div className="cell small-12 medium-7">
                  <form className="news-letter">
                    <input type="text" placeholder="Your Email Address" required/>
                    <button type="submit">+</button>
                  </form>
                </div>
              </div>
            </div>
            <div className="cell small-10 medium-5">
              <div className="grid-x">
                <div className="cell small-12 medium-6">
                  <ul className="footer-links">
                    <li><Link prefetch href={{ pathname: "/about" }}><a>About</a></Link></li>
                    <li><Link prefetch href={{ pathname: "/privacy-policy" }}><a>Privacy</a></Link></li>
                    <li><Link prefetch href={{ pathname: "/blog" }}><a>Blog</a></Link></li>
                  </ul>
                </div>
                <div className="cell small-12 medium-6">
                  <ul className="footer-links">
                    <li><Link prefetch href={{ pathname: "/faqs" }}><a>FAQs</a></Link></li>
                    <li><Link prefetch href={{ pathname: "/terms-of-service" }}><a>Terms of Service</a></Link></li>
                    <li><Link prefetch href={{ pathname: "/contact-us" }}><a>Contact Us</a></Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <hr className="cell small-12"/>
            <div className="cell small-12 disclaimer">
              <p>
                Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  nam. Sed et decore consul adolescens. Vel eripuit dissentiet ut, an his aperiam.Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  nam. Sed et decore consul adolescens. Vel eripuit dissentiet ut, an his aperiam. Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  nam. Sed et decore consul adolescens. 
              </p>
            </div>
            <div className="cell small-12 medium-8 copyright">
              <p>
                © 2018 VFD Microfinance Bank Ltd., All rights reserved.
              </p>
            </div>
            <div className="cell small-12 medium-4">
              <ul className="social-icons">
                <li><a href="https://www.facebook.com" className="facebook" target="_blank">Facebook</a></li>
                <li><a href="https://www.twitter.com" className="twitter" target="_blank">Twitter</a></li>
                <li><a href="https://www.linkedin.com" className="linkedin" target="_blank">LinkedIn</a></li>
                <li><a href="https://www.youtube.com" className="instagram" target="_blank">Instagram</a></li>
                <li><a href="https://www.youtube.com" className="youtube" target="_blank">YouTube</a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default withRouter(Footer)