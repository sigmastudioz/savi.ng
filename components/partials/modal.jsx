import React, { Component } from 'react'

class Modal extends Component {
  constructor (props) {
    super(props);
    this.state = {
      ...props
    };    
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ showModal: nextProps.showModal })
  }

  render () {
    const { showModal } = this.state 

    let modalCloseButton = []
    if (showModal) {
      modalCloseButton.push(<a key={`modal-close-button`} onClick={(e) => {
        if (this.props.closeModal) {
          this.props.closeModal(e)
        } else {
          e.preventDefault()
        }
      }} className="close-content" href="#"></a>)
    }

    return (
      <div className={`popup-module ${ showModal ?  "show-bg" : "" }`} data-animate-type={"fade"}>
        <div className="grid-x align-middle align-center-middle show-content">
          <div className="cell small-12 medium-8 large-6">
            {modalCloseButton}
            <div className="card white">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Modal