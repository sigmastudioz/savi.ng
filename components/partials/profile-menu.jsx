import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'

class ProfileMenu extends Component {
  constructor (props) {
    super(props); 
  }

  render () {
    const { router } = this.props
    const item = {
      firstName: 'Nonso',
      lastName: 'Okpala',
      imageUrl: 'https://randomuser.me/api/portraits/men/14.jpg'
    }

    const menus = [
      {
        title: "Profile",
        url: "/account/profile"
      },
      {
        title: "Withdrawals",
        url: "/account/profile/withdrawals"
      },
      {
        title: "My Cards",
        url: "/account/profile/my-cards"
      },
      {
        title: "My Banks",
        url: "/account/profile/my-banks"
      },
      {
        title: "Log out",
        url: "/account/profile/my-banks"
      }
    ]

    let img = null
    if (item.imageUrl) {
      img = <img src={require('../../assets/img/pp.jpg')} />
    } else {
      img = <p>{item.firstName.substring(0, 1)}</p>
    }

    return (
      <div className="card white">
        <div className="avatar align-middle align-center">{img}</div>
        <br/>
        <h3 className="profile-name">{ item.firstName } { item.lastName }</h3>
        <ul className="avatar-menu">
          {
            menus.map((menu, index) => {
              return (
                <li key={index}>
                  <Link prefetch href={{ pathname: menu.url }}><a className={`${router.pathname === menu.url? `active`: ``}`}>{menu.title}</a></Link>
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
}

export default withRouter(ProfileMenu)
