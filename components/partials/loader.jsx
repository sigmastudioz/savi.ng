import Loader from 'react-loaders'

export default ({ showLoading, size }) => {
  return (
    <div className="loadingOverlay">
      <div className="loaderStyle">
        <Loader type="line-scale" />
      </div>
    </div>
  )
}