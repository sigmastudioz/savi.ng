import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'

class Menu extends Component {
  render () {
    const { router } = this.props
    const menus = [
      {
        title: "My Goals",
        url: "/account/savings"
      },
      {
        title: "Contact Us",
        url: "/account/contact-us"
      },
      {
        title: "Referrals",
        url: "/account/referrals"
      },
      {
        title: "Settings",
        url: "/account/profile"
      }
    ]

    return (
      <div className="grid-x grid-padding-x site-menu text-align-center">
        {
          menus.map((menu, index) => {
            return (
              <div className="cell auto" key={index}>
                <Link prefetch href={{ pathname: menu.url }}><a className={`${router.pathname.indexOf(menu.url) !== -1? `active`: ``}`}>{menu.title}</a></Link>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default withRouter(Menu)