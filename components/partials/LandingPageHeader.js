import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'

class LandingPageHeader extends Component {

  render(){
    const { router } = this.props
    const menus = [
      {
        title: "About",
        url: "/about"
      },
      {
        title: "Features",
        url: "/features"
      },
      {
        title: "FAQ",
        url: "/faqs"
      },
      {
        title: "Blog",
        url: "#",
        type: "external"
      },
      {
        title: "Sign up",
        className: "button purple",
        url: "/signup"
      }
    ]

    return (
      <div className="top-bar" style={{backgroundColor: 'transparent', fontWeight: 'bold', padding: '30px'}}>
        <div className="top-bar-left">
            <ul className="dropdown menu" style={{backgroundColor: '#FAF5FF'}}>
                <li>
                  <img src={require('../../assets/img/saviLogo.svg')} style={{height : '50px'}}/>
                  <span className="badge badge-pill badge-danger">Beta</span>
                </li>
                {/* <li className="menu-text">Savi.ng</li> */}
            </ul>
        </div>
        <div className="top-bar-right">
          <ul className="menu" style={{backgroundColor: '#FAF5FF'}}>
            {
              menus.map((menu, index) => {
                if (menu && menu.type === 'externa,') {
                  return (
                    <li key={index}>
                      <a className={`${menu.className || ''} ${router.pathname === menu.url? `active`: ``}`}>{menu.title}</a>
                    </li>
                  )
                } else {
                  return (
                    <li key={index}>
                      <Link prefetch href={{ pathname: menu.url }}><a className={`${menu.className || ''} ${router.pathname === menu.url? `active`: ``}`}>{menu.title}</a></Link>
                    </li>
                  )
                }
                
              })
            }
          </ul>
        </div>
      </div>
    )
  }
}

export default withRouter(LandingPageHeader)