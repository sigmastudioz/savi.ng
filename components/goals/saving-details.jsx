import React, { Component } from 'react'
import _ from 'lodash'
import 'react-dates/initialize'
import moment from 'moment'
import { DateRangePicker } from 'react-dates'
import InputFormat from 'react-currency-format'

import Loader from '../partials/loader'
export const frequency_types = [
  {
    key: 'days',
    value: 'Daily'
  },
  {
    key: 'weeks',
    value: 'Weekly'
  },
  {
    key: 'months',
    value: 'Monthly'
  },
  {
    key: 'quarters',
    value: 'Quarterly'
  }
]

class Details extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputStates: {},
      ...props
    };

    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onFocus(event) {
    const target = event.target;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = true
      return updatedState;
    })
  }

  onBlur(event) {
    const target = event.target;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = false
      return updatedState;
    })
  }

  currencyFormatter (value) {
    var parts = value.toFixed(2).split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (+parts[1] ? "." + parts[1] : "");
  }

  calculateGoal() {
    const { periodic_amount, frequency, endDate,  startDate, periodic_amount_extra } = this.props

    if(periodic_amount && frequency && startDate && endDate) {
      const updatedFrequency = frequency_types[_.map(frequency_types, 'key').indexOf(frequency)];
      return periodic_amount_extra > 1 ? parseFloat(periodic_amount_extra) : (periodic_amount / (moment(endDate).diff(moment(startDate), (updatedFrequency && updatedFrequency.key) || 'days') || 1))
    }

    return
  }

  render () {
    const { title, description, handleValueChange, handleInputChange, startDate, endDate, frequency, focusedInput, onFocusChange, onDatesChange, showLoading, button } = this.props
    const updatedFrequency = frequency_types[_.map(frequency_types, 'key').indexOf(frequency)]

    return (
      <div className="grid-x align-top align-center">
        <div className="cell small-12 medium-10">
          <div className="grid-x grid-padding-x">
            <div className="cell small-12">
              {title}
              {description}
            </div>
            <div className="cell small-12 medium-6 input-selector">
              <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.saving_goal ? 'active' : ''}`}>I am saving for?</label>
              <input 
                type="text" 
                name="saving_goal"
                onKeyUp={handleInputChange}
                onFocus={this.onFocus} 
                onBlur={this.onBlur}/>
            </div>
            <div className="cell small-12 medium-6 input-selector">
              <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.periodic_amount ? 'active' : ''}`}>How much do you want to save?</label>
              {/*<input 
                type="number" 
                name="periodic_amount"
                min="0"
                max="1000000000000"
                onKeyUp={handleInputChange}
                onFocus={this.onFocus} 
              onBlur={this.onBlur} />*/}
              <InputFormat 
                prefix={'₦'}
                thousandSeparator={true}
                allowNegative={true}
                value={this.props.periodic_amount_format} 
                fixedDecimalScale={true}
                decimalScale={2}
                onValueChange={(values) => handleValueChange(values, 'periodic_amount', 'periodic_amount_format')}
              />
            </div>
            <div className="cell small-12 input-selector">
              <div className="grid-x grid-padding-x">
                <div className="cell small-6">
                  <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.card_number ? 'active' : ''}`}>Start Date</label>
                </div>
                <div className="cell small-6">
                  <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.card_number ? 'active' : ''}`}>End Date</label>
                </div>
              </div>

              <DateRangePicker
                openDirection={`down`}
                anchorDirection={`left`}
                startDate={startDate} // momentPropTypes.momentObj or null,
                startDateId="savi.ng_start_date_id" // PropTypes.string.isRequired,
                endDate={endDate} // momentPropTypes.momentObj or null,
                endDateId="savi.ng_end_date_id" // PropTypes.string.isRequired,
                onDatesChange={onDatesChange} // PropTypes.func.isRequired,
                focusedInput={focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                onFocusChange={onFocusChange} // PropTypes.func.isRequired,
              />
            </div>
            <div className="cell small-12 input-selector">
              <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.frequency ? 'active' : ''}`}>Saving Frequency</label>
              <select 
                name="frequency"
                onChange={handleInputChange} 
                onFocus={this.onFocus} 
                onBlur={this.onBlur}>
                {
                  frequency_types.map((data, index) => {
                    return (<option value={data.key} key={index}>{data.value}</option>)
                  })
                }
              </select>
            </div>
            <div className="cell small-6 input-selector">
              <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.periodic_amount_extra ? 'active' : ''}`}><strong>{(updatedFrequency && updatedFrequency.value) || 'Daily'} Budget</strong>? You can adjust it below.</label>
              {/*<input 
                type="number"
                min={0}
                name="periodic_amount_extra"
                defaultValue={this.calculateGoal() || 0} 
                onFocus={this.onFocus} 
              onBlur={this.onBlur}/>*/}
              <InputFormat 
                prefix={'₦'}
                thousandSeparator={true}
                allowNegative={true}
                fixedDecimalScale={true}
                decimalScale={2}
                onValueChange={(values) => handleValueChange(values, 'periodic_amount_extra', 'periodic_amount_extra_format')}/>
            </div>
            <div className="cell small-6 input-selector">
              <label className={`text-align-left ${this.state.inputStates && this.state.inputStates.frequency ? 'active' : ''}`}>Total</label>
              <InputFormat 
                prefix={'₦'}
                thousandSeparator={true}
                allowNegative={true}
                disabled={true}
                value={this.calculateGoal() || 0} 
                fixedDecimalScale={true}
                decimalScale={2}
                onValueChange={(values) => handleValueChange(values, 'periodic_amount_extra', 'periodic_amount_extra_format')}/>
            </div>
            { button }
          </div>
        </div>
        {
          showLoading ? (<Loader showLoading={showLoading} size={100}/>) : null
        }
      </div>
    )
  }
}

export const GoalDetails = Details