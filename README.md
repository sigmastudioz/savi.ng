[![Deploy to now](https://deploy.now.sh/static/button.svg)](https://deploy.now.sh/?repo=https://github.com/zeit/next.js/tree/master/examples/custom-server-express)

# Custom Express Server example

## How to use

### Running App

Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```
