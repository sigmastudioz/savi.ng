import '../assets/css/style.scss'

import React, { Component } from 'react'
import Head from 'next/head'
import Menu from '../components/partials/menu'
import Header from '../components/partials/header' 
import SideNav from '../components/partials/side-nav' 

export default class DefaultLayout extends Component {
  constructor (props) {
    super(props);
    this.state = {
      menuStatus: false,
    };    

    this.openMenu = this.openMenu.bind(this)
    this.closeMenu = this.closeMenu.bind(this)
  }

  openMenu(e) {
    e.preventDefault()
    this.setState({ menuStatus: true })
  }

  closeMenu(e) {
    e.preventDefault()
    this.setState({ menuStatus: false })
  }

  render () {
    const { menuStatus } = this.state;
    const { children, className = '', url, hideNav = false } = this.props

    return (
      <div>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
          <link href="/_next/static/style.css" rel="stylesheet"/>
        </Head>
        <div className="off-canvas-wrapper" id="saving-ng-body">
          <div className="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            <div className={`off-canvas position-left is-transition-push ${menuStatus? 'is-open' : 'is-closed'}`} id="offCanvasLeft" data-off-canvas>
              <h1 className="text-align-center funds-update">
                You’ve saved <span className="funds-stat">₦42,000.00</span>
              </h1>
              <Menu/>
            </div>
            <div className={`off-canvas-content ${menuStatus? 'is-open-left has-transition-push has-position-left' : ''}`} data-off-canvas-content>
              <div className="grid-container fluid">
                <div className="grid-x wrapper">
                  
                  {/* { (hideNav) ? null : <SideNav url={url}/> } */}
                  <div className="cell auto">
                  { (hideNav) ? null : <Header menuStatus={menuStatus} openMenu={this.openMenu} closeMenu={this.closeMenu}/> }
                    <div className={className}>{ children }</div>
                  </div>
                </div>
                {
                  menuStatus? (
                    <div className="canvas-overlay" onClick={(e) => this.closeMenu(e)}>
                    
                    </div>
                  ) : null
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}