import '../assets/css/style.scss'

import React, { Component } from 'react'
import Head from 'next/head'

export default class DefaultLayout extends Component {
  render () {
    const { children, className = ''} = this.props

    return (
      <div>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
          <link href="/_next/static/style.css" rel="stylesheet"/>
        </Head>
        <div className="off-canvas-wrapper" id="saving-ng-body">
          <div className="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            {/*<div className="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
              
            </div>*/}
            <div className="off-canvas-content" data-off-canvas-content>
              <div className="grid-container fluid">
                <div className="grid-x wrapper">                  
                  <div className="cell auto">
                    <div className={className}>{ children }</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}