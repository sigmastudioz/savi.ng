import React, { Component } from 'react'
import LandingPageLayout from '../layouts/landing'
import LandingPageHeader from '../components/partials/LandingPageHeader'
import Footer from '../components/partials/footer' 

export default class PrivacyPolicy extends Component {
  render () {
    const { url } = this.props

    return (
      <div>
          <LandingPageLayout url={url} hideNav={true} className="home-page">
            <div className="main-banner">
            <LandingPageHeader style={{ backgroundColor: '#FAF5FF'}}/>
              <div className="grid-x align-middle align-center-middle sub-page-splash">
                <div className="cell small-12">
                  <h2>Privacy Policy</h2>
                </div>
              </div>
              <div className="grid-x align-middle align-center-middle sub-page-splash-alt">
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-middle">
                    <div className="cell small-12">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer/>
          </LandingPageLayout>
          
      </div>
      
    )
  }
}