import React, { Component } from 'react'
import DefaultLayout from '../layouts/default'
import { Link } from 'next-routes'

export default class MyBanksPage extends Component {
  render () {
    const { url } = this.props
    return (
      <DefaultLayout url={url} hideNav={true} className="stretch-page">
        <div className="grid-x align-middle align-center-middle start-page">
          <div className="cell small-12 medium-9 large-5 text-align-center">
            <form>
              <div className="card white">
                <div className="grid-x grid-padding-x align-center">
                  <div className="cell small-12">
                    <h1>One Step Verification</h1>
                    <p>Enter the code we sent to your phone number</p>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                  </div>
                  <div className="cell small-10">
                    <div className="grid-x grid-padding-x">
                      <div className="cell small-2">
                        <input type="text"/>
                      </div>
                      <div className="cell small-2">
                        <input type="text" />
                      </div>
                      <div className="cell small-2">
                        <input type="text" />
                      </div>
                      <div className="cell small-2">
                        <input type="text" />
                      </div>
                      <div className="cell small-2">
                        <input type="text" />
                      </div>
                      <div className="cell small-2">
                        <input type="text" />
                      </div>
                    </div>
                  </div>
                </div>
                <br/>
                <br/>
                <div className="grid-x grid-padding-x">
                  <div className="cell small-6">
                    <button className="button purple expanded">
                      VERIFY
                    </button>
                  </div>
                  <div className="cell small-6 text-align-center">
                    <p>Didn’t get the code?</p>
                    <a>Resend</a>
                  </div>
                </div>
                <br/>
                <br/>
                <br/>
              </div>
            </form>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}