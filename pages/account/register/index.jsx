import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'

export default class RegistrationPage extends Component {
  render () {
    const { url } = this.props
    return (
      <DefaultLayout url={url}>
        
      </DefaultLayout>
    )
  }
}