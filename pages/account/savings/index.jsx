import React, { Component } from 'react'
import Link from 'next/link'
import DefaultLayout from '../../../layouts/default'
import TeamCards from '../../../components/teams/cards'
import GoalSummary from '../../../components/teams/goal-summary'
import TeamContributors from '../../../components/teams/contributors'

class SavingsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeGoal: "1111111111111",
      goalsList: [],
      contributors: [],  
      cardLists: [],
      showLoading: false,
      refresh: true,
      goal: null
    };

    this.setActiveGoal = this.setActiveGoal.bind(this);
  }

  setActiveGoal(e, item) {
    const vm = this
    e.preventDefault()
    if (this.state.activeGoal !== item.id && !vm.state.showLoading) {
      vm.setState({
        activeGoal: item.id,
        refresh: false,
        showLoading: true
      })

      setTimeout(() => {
        vm.setState({
          showLoading: false,
          refresh: true,
          goal: item
        })
      }, 5000)
    }
  }

  render () {
    const { route } = this.props
    // const { goalsList, contributors,  cardLists} = this.state
    const sampleChart = [
      {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
    ]

    const contributors = [
      {
        firstName: "Withdrawal",
        lastName: "Processed",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "2,000",
        type: "positive"
      },
      {
        firstName: "Withdrawal",
        lastName: "Processed",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "2,000",
        type: "negative"
      },
      {
        firstName: "Credit",
        lastName: "Transaction",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "2,000",
        type: "negative"
      },
      {
        firstName: "Credit",
        lastName: "Transaction",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "2,000",
        type: "positive"
      },
      {
        firstName: "Withdrawal",
        lastName: "Processed",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "2,000",
        type: "positive"
      }
    ]

    const goalsList = [
      {
        id: "1111111111111",
        title: "New iPhone",
        interest_returns: "35,000",
        amount_saved: "100,000",
        total_amount: "500,000",
        progress_type: "success",
        progress: "50%",
        saving_date: "28 Jul, 2018",
        frequency: "Weekly",
        frequency_returns: "15,000",
        last_digits: 3575,
        contributions: contributors,
        stats: sampleChart
      },
      {
        id: "222222222222",
        title: "2022 World Cup",
        interest_returns: "15,000",
        amount_saved: "50,000",
        total_amount: "250,000",
        progress_type: "alert",
        progress: "50%",
        saving_date: "15 Jul, 2018",
        frequency: "Weekly",
        frequency_returns: "15,000",
        last_digits: 2242,
        contributions: contributors,
        stats: sampleChart
      },
      {
        id: "333333333333",
        title: "Macbook Pro",
        interest_returns: "10,000",
        amount_saved: "200,000",
        total_amount: "800,000",
        progress_type: "success",
        progress: "50%",
        saving_date: "20 Aug, 2018",
        frequency: "Weekly",
        frequency_returns: "15,000",
        last_digits: 4423,
        contributions: contributors,
        stats: sampleChart
      }
    ]

    const cardLists = [
      {
        id: "111111111",
        bank_name: "Zenit Bank",
        last_digits: 2242
      },
      {
        id: "222222222",
        bank_name: "UBA",
        last_digits: 4423
      },
      {
        id: "333333333",
        bank_name: "Union Bank",
        last_digits: 3321
      },
      {
        id: "444444444",
        bank_name: "GT Bank",
        last_digits: 3575
      }
    ]

    const savingsState = (goalsList && goalsList.length > 0) ?
    (
      <div>
        <div className="grid-x grid-padding-x">
          <div className="cell small-8">
            <h1 className="title">
              My Goals
            </h1>
          </div>
          <div className="cell small-4 text-align-right">
            <Link prefetch href={{ pathname: '/account/savings/my-goals/add' }}><a className="title-link">+ Add Goal</a></Link>
          </div>
        </div>
        <div className="grid-x grid-padding-x goals-slider">
          {
            goalsList.map((team, index) => {
              return (
                <div className="cell medium-12 large-4" key={index}>
                  <TeamCards item={team} 
                    cardLists={cardLists}
                    activeGoal={this.state.activeGoal} 
                    setActiveGoal={this.setActiveGoal}/>
                </div>
              )
            })
          }
        </div>
        
        <GoalSummary {...this.state} goal={this.state.goal || goalsList[0]}/>

        {/*<div className="grid-x grid-padding-x">
          <div className="cell medium-12 large-6">
            <div className="grid-x grid-padding-x">
              <div className="cell small-12 medium-6">
                <div className="card purple">
                  <h4>TOTAL BALANCE</h4>
                  <p className="funds">₦410,042.02</p>
                </div>
              </div>
              <div className="cell small-12 medium-6">
                <div className="card white">
                  <h4>TOTAL Amount Saved</h4>
                  <p className="funds">₦350,000</p>
                </div>
              </div>
            </div>

            <div className="grid-x grid-padding-x">
              <div className="cell small-12 medium-6">
                <div className="card white">
                  <h4>CURRENT RETURNS</h4>
                  <p className="funds">₦60,042.02</p>
                </div>
              </div>
              <div className="cell small-12 medium-6">
                <div className="card white">
                  <h4>LIFETIME RETURNS</h4>
                  <p className="funds">₦15,022.80</p>
                </div>
              </div>
            </div>

            <div className="grid-x grid-padding-x">
              <div className="cell small-12">
                <div className="card white">
                  <h4>TEAM Account Summary</h4>
                  <p className="funds">₦60,042.02</p>
                </div>
              </div>
            </div>
          </div>
          <div className="cell medium-12 large-6">
            <div className="card white contributions">
              <div className="grid-x grid-padding-x">
                <div className="cell small-6">
                  <h4>OVERALL CONTRIBUTIONS</h4>
                </div>
                <div className="cell small-6 text-align-right">
                  
                </div>
              </div>
              {
                contributors.map((team, index) => {
                  return (
                    <TeamContributors item={team} key={index}/>
                  )
                })
              }
            </div>
          </div>
            </div>*/}
      </div>
    ) :
    (
      <div className="grid-x align-middle align-center-middle start-page">
        <div className="cell small-12 medium-3 text-align-center">
          <img src={require('../../../assets/img/add-goal-start.png')} className="start-icon" height="240"/>
          <p className="spacer">
            You have no savings yet. Add personal goals by clicking on the button below
          </p>
          <Link prefetch href={{ pathname: '/account/savings/my-goals/add' }}>
            <a className="button purple expanded">
              ADD GOAL
            </a>
          </Link>
        </div>
      </div>
    )

    return (
      <DefaultLayout className="team-wrapper" url={route}>
        {
          savingsState
        }
      </DefaultLayout>
    )
  }
}

export default SavingsPage
