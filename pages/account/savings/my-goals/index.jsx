import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'
import moment from 'moment'
import DefaultLayout from '../../../../layouts/default'

class ManageGoalsDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focusedInput: 'startDate',
      currentStep: 0,
      startDate: moment(),
      debit_card: "",
      frequency: 'daily',
      saving_goal: null,
      periodic_amount: 0,
      periodic_amount_extra: 0,
      endDate: moment(),
      processed: false,
      card_number: null,
      expire_date: moment(),
      cvc: null,
      showLoading: false,
      ...props
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState[name] = value
      return updatedState;
    })
  }

  render () {
    const { url, params } = this.props
    return (
      <DefaultLayout url={url}>
        <form name="manage-goal">
          <div className="grid-x grid-padding-x breadcrumbs">
            <div className="cell small-12">
              <h1>
                <Link prefetch href={{ pathname: '/account/savings' }}><a className="inactive">My Savings</a></Link> > <span className="active">Manage Goal</span>
              </h1>
            </div>
          </div>
          

          <div className="grid-x grid-padding-x">
            <div className="cell small-12 medium-12 large-6">
              <div className="card white">
                <div className="grid-x grid-padding-x align-center">
                  <div className="cell small-10 medium-8">
                    <div className="grid-x grid-padding-x">
                      <div className="cell small-12 input-selector">
                        <label className="text-align-left">Change goal amount?</label>
                        <input type="text" name="final_amout"/>
                      </div>
                      <div className="cell small-12 input-selector">
                        <label className="text-align-left">Change Periodic Amount?</label>
                        <input type="text" name="peridic_amount"/>
                      </div>
                      <div className="cell small-12 input-selector">
                        <label className="text-align-left">Change the frequency of your Savings</label>
                        <select>
                          <option value="monthly">Monthly Savings</option>
                        </select>
                      </div>
                      <div className="cell small-12 input-selector">
                        <label className="text-align-left">Change Withdrawal Date</label>
                        <input type="date" name="targe_amount"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="cell small-12 medium-12 large-6">
              <div className="card white contributions">
                <div className="grid-x grid-padding-x align-center">
                  <div className="cell small-10 medium-8">
                    <div className="grid-x grid-padding-x">
                      <div className="cell small-12 input-selector">
                        <label className="text-align-left">Change debit card assigned?</label>
                        <select>
                          <option value="monthly">GTBank VISA **3575</option>
                        </select>
                      </div>
                      <div className="cell small-12">
                        <button className="button purple expanded">
                          SAVE CHANGES
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </DefaultLayout>
    )
  }
}

export default withRouter(ManageGoalsDetails)
