import React, { Component } from 'react'
import Link from 'next/link'
import 'react-dates/initialize'
import { DateRangePicker } from 'react-dates'
import moment from 'moment'
import _ from 'lodash'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'


import DefaultLayout from '../../../../layouts/default'
import Stepper from '../../../../components/teams/stepper'
import AddCard from '../../../../components/teams/add-card'
import Loader from '../../../../components/partials/loader'
import { GoalDetails,  frequency_types} from '../../../../components/goals/saving-details'


const AddGoalSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
});

const Step2 = ({ handleInputChange, nextStep, debit_card, showLoading }) => {
  return (
    <div className="grid-x align-top align-center start-page">
      <div className={`cell small-10 medium-8`}>
        <div className="grid-x grid-padding-x">
          <div className="cell small-12">
            <h2>Add Payment</h2>
          </div>
          <div className={`cell input-selector small-12`}>
            <label className="text-align-left">Do you have a debit card saved for this goal?</label>
            <select 
              name="debit_card"
              onChange={handleInputChange}>
              <option value="">Skip to create card at dashboard</option>
              <option value="add-new-card">Add New Debit Card</option>
              <option value="weekly">GTBank VISA **3575</option>
              <option value="monthly">UBA VISA **4242</option>
              <option value="yearly">Union Bank VISA **0009</option>
            </select>
          </div>
          { 
            (debit_card === "add-new-card")? (
              <div className="cell small-12">
                <AddCard  
                  {...this}
                  handleInputChange={handleInputChange}
                  />
              </div>
            ) : null
          }
          <div className="cell small-12">
            <button className="button purple expanded max-340" onClick={(e) => nextStep(e, 2)}>
              SAVE
            </button>
          </div>
        </div>
      </div>
      {
        showLoading ? (<Loader showLoading={showLoading} size={100}/>) : null
      }
    </div>
  )
}

export default class MyGoal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0,
      startDate: moment(),
      debit_card: "",
      frequency: 'days',
      saving_goal: null,
      periodic_amount: 1,
      periodic_amount_format: 1,
      periodic_amount_extra: 1,
      periodic_amount_extra_format: 1,
      endDate: moment(),
      processed: false,
      card_number: null,
      expire_date: moment(),
      cvc: null,
      showLoading: false,
      ...props
    }

    this.nextStep = this.nextStep.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValueChange = this.handleValueChange.bind(this)
    this.onFocusChange = this.onFocusChange.bind(this)
    this.onDatesChange = this.onDatesChange.bind(this)
  }

  nextStep(e, stepIndex) {
    
    e.preventDefault()
    if (!this.state.processed) {
      this.setState({
        currentStep: stepIndex
      })
    }
  }

  handleChange(date) {
    this.setState({
      startDate: date
    })
  }

  saveGoalOnServer (e) {
    e.preventDefault()
    // fetch().then((data) => {
    //   if (data.status) {
    //     this.setState({
    //       processed: true
    //     })
    //   } else {
    //     // show Error modal, show data.meessages
    //   }
    // }).catch((e) => {
    //   // show error Modal. Show server response
    // })
    this.setState({
      showLoading: true
    })

    setTimeout(() => {
      this.setState({
        showLoading: false,
        processed: true
      })
    }, 5000)
  }

  currencyFormatter (value) {
    var parts = value.toFixed(2).split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (+parts[1] ? "." + parts[1] : "");
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState[name] = value
      return updatedState;
    })
  }

  handleValueChange (values, property, property_format) {
    const {formattedValue, value} = values
    var update = {}
    update[property] = value
    update[property_format] = formattedValue
    this.setState(update)
  }

  onFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  onDatesChange({ startDate, endDate }) {
    this.setState({ startDate, endDate })
  }

  calculateGoal() {
    const { periodic_amount, frequency, endDate,  startDate, periodic_amount_extra } = this.state

    if(periodic_amount && frequency && startDate && endDate) {
      const updatedFrequency = frequency_types[_.map(frequency_types, 'key').indexOf(frequency)];
      return (periodic_amount / (moment(endDate).diff(moment(startDate), (updatedFrequency && updatedFrequency.key) || 'days') || 1))
    }

    return
  }

  render () {
    const { url, currentStep, frequency, startDate, endDate, saving_goal, periodic_amount, periodic_amount_extra, debit_card, processed, showLoading } = this.state
    const updatedFrequency = frequency_types[_.map(frequency_types, 'key').indexOf(frequency)]
    const frequencyType = (updatedFrequency && updatedFrequency.value) || 'Daily'

    const startPage = (
      <div className="grid-x align-middle align-center-middle start-page">
        <div className="cell small-8 medium-6 large-4 text-align-center">
          <img src={require('../../../../assets/img/add-goal-start.png')} className="start-icon" height="240"/>
          <p className="spacer">
            You have no savings yet. Add personal goals by clicking on the button below
          </p>
          <button className="button purple expanded max-340" onClick={(e) => this.nextStep(e, 0)}>
            ADD GOAL
          </button>
        </div>
      </div>
    )

    const endPage = processed ? (
      <div className="grid-x align-top align-center start-page">
        <div className="cell small-8 medium-6 text-align-center">
          <h2>Goal created successfully</h2>
          <img src={require('../../../../assets/img/verify-icon.png')} style={{height: '125px'}}/>
          { 
            (debit_card === "")? (
              <p className="spacer">
                If you didn’t assign a debit card to this goal, please do so at “My Card” under “Settings”. If otherwise, kindly ignore this message.
              </p>
            ): null 
          }
          <Link prefetch href={{ pathname: '/account/savings'}}>
            <a className={`button purple expanded max-340`}>
              PROCEED TO DASHBOARD
            </a>
          </Link>
        </div>
      </div>
    ) : (
      <div className="grid-x align-top align-center start-page">
        <div className="cell small-8 medium-6 text-align-center">
          <h2>Goal summary</h2>
          <p className="summary">
            <small>Check to make sure everything is ok before submission. Click on the steps above to edit your choices. If everything looks good, click confirm to save goal.</small>
          </p>
          <hr/>
          <p>
            <small><strong>Saving for:</strong> { saving_goal || '---'}</small>
          </p>
          <p>
            <small><strong>Start Date:</strong> { moment(startDate).format('MM/DD/YYYY') || '---'} - <strong>End Date:</strong> {moment(endDate).format('MM/DD/YYYY') || '---'}</small>
          </p>
          <p>
            <small><strong>Total Savings:</strong> ₦{this.currencyFormatter(parseFloat(periodic_amount)|| 0)}</small>
          </p>
          <p>
            <small><strong>Frequency:</strong> {frequencyType}</small>
          </p>
          <p>
            <small><strong>Extra Saving amount:</strong> ₦{this.currencyFormatter(parseFloat(periodic_amount_extra || 0))}</small>
          </p>
          <button className="button purple expanded max-340" onClick={(e) => this.saveGoalOnServer(e)} disabled={showLoading}>
            CONFIRM
          </button>
        </div>
        {
          showLoading ? (<Loader showLoading={showLoading} size={100}/>) : null
        }
      </div>
    )

    const extraModule = (
      <div className={`grid-x align-middle align-center-middle savings-calculator ${currentStep >= 0 ?  'right-fade' : 'hide'}`}>
        <div className="cell small-12 text-align-center">
          <h2>Savings Calculator</h2>
          <p className="spacer">
            Total amount: ₦{ this.currencyFormatter(parseInt(periodic_amount || 0))}
          </p>
          <p className="spacer">
            {periodic_amount_extra > 0? 'Your custom extra' : 'Required'} minimum {frequencyType} Budget: ₦{ this.currencyFormatter((this.calculateGoal() || 0) + parseInt(periodic_amount_extra || 0)) }
          </p>
        </div>
      </div>
    )
    // extraModule={!processed ? extraModule : null}

    return (
      <DefaultLayout url={url}>
        <form name="add-goal">
          <Stepper startPage={startPage} currentStep={currentStep} nextStep={this.nextStep}>
            <GoalDetails {...this.state} 
              title={(
                <h2>
                  Information Details
                </h2>
              )}
              handleValueChange={this.handleValueChange} 
              handleInputChange={this.handleInputChange} 
              nextStep={this.nextStep}
              calculateGoal={this.calculateGoal() || 0}
              onFocusChange={this.onFocusChange} 
              onDatesChange={this.onDatesChange}
              button={(
                <div className="cell small-12 text-align-center">
                  <button className="button purple expanded max-340" onClick={(e) => this.nextStep(e, 1)} disabled={(periodic_amount_extra || 0) > periodic_amount}>
                    NEXT
                  </button>
                </div>
              )}/>
            <Step2 {...this.state} 
              handleInputChange={this.handleInputChange} 
              nextStep={this.nextStep}/>
            {endPage}
          </Stepper>
        </form>
      </DefaultLayout>
    )
  }
}