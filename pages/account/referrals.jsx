import React, { Component } from 'react'
import DefaultLayout from '../../layouts/default'

export default class Referrals extends Component {
  render () {
    const { url } = this.props

    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-top align-center start-page">
          <div className="cell small-12 medium-12 large-8 text-align-center">
            <form name="invite">   
              <div className="grid-x grid-padding-x">
                <div className="cell small-12">
                  <div className="card white">
                    <div className="grid-x grid-padding-x">
                      <div className="cell small-12">
                        <h1 className="text-align-center">Referral Link</h1>
                        <p className="text-align-center summary">
                          Share the good news, invite friends and family using link below 
                        </p>
                      </div>
                      
                      <div className="cell small-12">
                        <div className="grid-x grid-padding-x align-center-middle earned-slots-left">
                          <div className="cell small-12 medium-6 text-align-center">
                            <h2>
                              <small>₦</small>1,000
                            </h2>
                            <p>
                              Referral Amount Earned
                            </p>
                          </div>
                          <div className="cell small-12 medium-6 text-align-center divider">
                            <h2>
                              90
                            </h2>
                            <p>
                              Referral Slots Left
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="cell small-12">
                        <div className="grid-x grid-padding-x align-center">
                          <div className="cell small-12 medium-8 large-6">
                            <input type="text" placeholder="https://www.savi.ng/xYg3hz68"/>
                            <br/>
                            <button className="button purple expanded">
                              <strong>COPY LINK</strong>
                            </button>
                            <br/>
                          </div>
                        </div>
                      </div>

                      <div className="cell small-12">
                        <div className="grid-x grid-padding-x align-center">
                          <div className="cell small-12 medium-6">
                            <ul className="social-links align-center-middle">
                              <li>Share via</li>
                              <li>
                                <a href="https://www.facebook.com" className="facebook" target="_blank">Facebook</a>
                              </li>
                              <li>
                                <a href="https://www.twitter.com" className="twitter" target="_blank">Twitter</a>
                              </li>
                              <li>
                                <a href="https://www.whatsapp.com" className="whatsapp" target="_blank">WhatsApp</a>
                              </li>
                              <li>
                                <a href="mailto:email@email.com" className="email" target="_blank">Email</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>   
              </div>
            </form>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}