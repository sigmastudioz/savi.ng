import React, { Component } from 'react'
import DefaultLayout from '../../layouts/default'

export default class ContactUs extends Component {
  render () {
    const { url } = this.props

    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-top align-center start-page">
          <div className="cell small-12 medium-12 large-8 text-align-center">
            <form name="contact-us">   
              <div className="grid-x grid-padding-x">
                <div className="cell small-12">
                  <div className="card white">
                    <div className="grid-x grid-padding-x align-center">
                      <div className="cell small-12 medium-10">
                        <p className="text-align-center summary">
                          Feel free to send us a message if you need help. 
                          You can also reach us on <a className="purple-text" href="tel:+23409087482189">0908-748-2189</a><br></br>
                          or <a className="purple-text" href="tel:+23409087482190">0908-748-2190</a>
                        </p>
                        <br/>
                        <br/>
                      </div>

                      <form className="cell small-12" name="signup">
                        <div className="grid-x grid-padding-x align-center">
                          <div className="cell small-12 medium-8 large-8">
                            <div class="cell small-12 input-selector">
                              <label class="text-align-left ">Current Issue</label>
                                <select name="issue">
                                  <option value="bug">Report a Bug</option>
                                  <option value="app">App Issue</option>
                                  <option value="payment">Payment Issue</option>
                                  <option value="else">Something Else</option>
                                </select>
                              </div>
                            <br/>
                            <textarea placeholder="Type in your message here" name="message"></textarea>
                            <br/>
                            <button className="button purple expanded">
                              <strong>SEND MESSAGE</strong>
                            </button>
                          </div>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>   
              </div>
            </form>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}