import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import DefaultLayout from '../../../layouts/default'
import TeamCards from '../../../components/teams/cards'
import TeamContributors from '../../../components/teams/contributors'

export default class TeamsPage extends Component {
  render () {
    const { url } = this.props

    const teamCardList = [
      {
        card_type: "alternate",
        title: "House Rent",
        interest_returns: "+ ₦35,000",
        amount_saved: "₦100,000",
        total_amount: "₦500,000",
        progress_type: "success",
        progress: "50%",
        saving_date: "28 Jul, 2018",
        participants: "2",
        frequency: "Weekly",
        path: {
          alias: 'team-saving-details',
          params: {
            details: 231132
          }
        }
      },
      {
        card_type: "white",
        title: "Office End of Year Party",
        interest_returns: "+ ₦15,000",
        amount_saved: "₦50,000",
        total_amount: "₦250,000",
        progress_type: "alert",
        progress: "50%",
        saving_date: "15 Jul, 2018",
        participants: "11",
        frequency: "Monthly",
        path: {
          alias: 'team-saving-details',
          params: {
            details: 1131132
          }
        }
      },
      {
        card_type: "white",
        title: "Vacation",
        interest_returns: "+ ₦10,000",
        amount_saved: "₦200,000",
        total_amount: "₦800,000",
        progress_type: "success",
        progress: "50%",
        saving_date: "20 Aug, 2018",
        participants: "2",
        frequency: "Weekly",
        path: {
          alias: 'team-saving-details',
          params: {
            details: 331132
          }
        }
      }
    ]

    const contributors = [
      {
        firstName: "Amanda",
        lastName: "Thompson",
        imageUrl: "https://randomuser.me/api/portraits/women/94.jpg",
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      },
      {
        firstName: "Bisola",
        lastName: "Adebowale",
        imageUrl: "https://randomuser.me/api/portraits/women/14.jpg",
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "negative"
      },
      {
        firstName: "Philips",
        lastName: "Thomas",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      },
      {
        firstName: "Emeka",
        lastName: "Timothy",
        imageUrl: "https://randomuser.me/api/portraits/men/14.jpg",
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      },
      {
        firstName: "Chris",
        lastName: "Nwankwo",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      }
    ]
    return (
      <DefaultLayout className="team-wrapper" url={url}>
        <div className="grid-x grid-padding-x">
          <div className="cell small-8">
            <h1 className="title">
              Team Savings
            </h1>
          </div>
          <div className="cell small-4 text-align-right">
            <Link route={'team-create'}><a className="title-link">+ Create New</a></Link>
          </div>
        </div>
        <div className="grid-x grid-padding-x">
          {
            teamCardList.map((team, index) => {
              return (
                <div className="cell medium-12 large-4" key={index}>
                  <TeamCards item={team}/>
                </div>
              )
            })
          }
        </div>

        <div className="grid-x grid-padding-x">
          <div className="cell medium-12 large-6">
            <div className="grid-x grid-padding-x">
              <div className="cell small-12 medium-6">
                <div className="card purple">
                  <h4>TOTAL BALANCE</h4>
                  <p className="funds">₦410,042.02</p>
                </div>
              </div>
              <div className="cell small-12 medium-6">
                <div className="card white">
                  <h4>TOTAL Amount Saved</h4>
                  <p className="funds">₦350,000</p>
                </div>
              </div>
            </div>

            <div className="grid-x grid-padding-x">
              <div className="cell small-12 medium-6">
                <div className="card white">
                  <h4>CURRENT RETURNS</h4>
                  <p className="funds">₦60,042.02</p>
                </div>
              </div>
              <div className="cell small-12 medium-6">
                <div className="card white">
                  <h4>LIFETIME RETURNS</h4>
                  <p className="funds">₦15,022.80</p>
                </div>
              </div>
            </div>

            <div className="grid-x grid-padding-x">
              <div className="cell small-12">
                <div className="card white">
                  <h4>TEAM Account Summary</h4>
                  <p className="funds">₦60,042.02</p>
                </div>
              </div>
            </div>
          </div>
          <div className="cell medium-12 large-6">
            <div className="card white contributions">
              
              <div className="grid-x grid-padding-x">
                <div className="cell small-6">
                  <h4>OVERALL CONTRIBUTIONS</h4>
                </div>
                <div className="cell small-6 text-align-right">
                  
                </div>
              </div>
                {
                  contributors.map((team, index) => {
                    return (
                      <TeamContributors item={team} key={index}/>
                    )
                  })
                }
            </div>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}