import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'
import Router from 'next/router'
import DefaultLayout from '../../../layouts/default'
import TeamContributors from '../../../components/teams/contributors'

class TeamSavingDetails extends Component {
  render () {
    const { url } = this.props
    
    const contributors = [
      {
        firstName: "Withdrawal",
        lastName: "Processed",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      },
      {
        firstName: "Withdrawal",
        lastName: "Processed",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "negative"
      },
      {
        firstName: "Credit",
        lastName: "Transaction",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "negative"
      },
      {
        firstName: "Credit",
        lastName: "Transaction",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      },
      {
        firstName: "Withdrawal",
        lastName: "Processed",
        imageUrl: null,
        saving_date: "30 Jun, 2018",
        contribution: "₦2,000",
        type: "positive"
      }
    ]

    return (
      <DefaultLayout url={url}>
        <div className="grid-x grid-padding-x">
          <div className="cell small-8">
            <h1>
              2022 World Cup
            </h1>
          </div>
          <div className="cell small-4 text-align-right">
            <Link route={'team-create'}><a>Settings</a></Link>
          </div>
        </div>

        <div className="grid-x grid-padding-x">
          <div className="cell small-6">
            <div className="grid-x grid-padding-x">
              <div className="cell small-6">
                <div className="card white">

                </div>
              </div>
              <div className="cell small-6">
                <div className="card white">
                
                </div>
              </div>

              <div className="cell small-12">
                <div className="card white">
                  <h4>SAVINGS PERFORMANCE</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="cell small-6">
            <div className="card white contributions">
              
              <div className="grid-x grid-padding-x">
                <div className="cell small-6">
                  <h4>RECENT CONTRIBUTIONS</h4>
                </div>
                <div className="cell small-6 text-align-right">
                  
                </div>
              </div>
                {
                  contributors.map((team, index) => {
                    return (
                      <TeamContributors item={team} key={index}/>
                    )
                  })
                }
            </div>
            <div className="grid-x grid-padding-x">
                <div className="cell small-6">
                  <div className="card white">
                    <h4>SAVINGS FREQUENCY</h4>
                  </div>
                </div>
                <div className="cell small-6">
                  <div className="card white">
                    <h4>DEBIT CARD ASSIGNED</h4>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}

export default withRouter(TeamSavingDetails)
