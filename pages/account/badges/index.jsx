import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'

export default class BadgesPage extends Component {
  render () {
    const { url } = this.props
    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-middle align-center-middle start-page">
          <div className="cell small-12 medium-3 text-align-center">
            <img src={require('../../../assets/img/add-badge-start.png')} className="start-icon" height="240"/>
            <p className="spacer">
              You have no badges yet. Save consistently to earn new badges and claim rewards.
            </p>
            <button className="button purple expanded">
              ADD FUND
            </button>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}