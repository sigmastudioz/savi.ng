import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'

export default class MyBanksPage extends Component {
  render () {
    const { url } = this.props
    return (
      <DefaultLayout url={url} hideNav={true}>
        <div className="grid-x align-middle align-center-middle start-page">
          <div className="cell small-12 medium-6 text-align-center">
            <img src={require('../../../assets/img/logo.png')} className="start-icon" height="240"/>
            <p className="spacer">
              You have no banks saved. Add your banks to withdraw your personal savings. 
            </p>
            <button className="button purple expanded">
              ADD BANK
            </button>
          </div>
          <div className="cell small-12 medium-6 text-align-center login-splash-image" 
            style={{
              backgroundImage: "url(`https://i.eurosport.com/2018/06/06/2349745-48893670-2560-1440.jpg`)",
              backgroundSize: "cover",
              backgroundPosition: "center",
              minHeight: "100vh"
            }}>

          </div>
        </div>
      </DefaultLayout>
    )
  }
}