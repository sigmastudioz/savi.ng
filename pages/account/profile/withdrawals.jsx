import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'
import WithDrawalCards from '../../../components/teams/cards'
import ProfileMenu from '../../../components/partials/profile-menu'

export default class MyWithDrawals extends Component {
  constructor(props) {
    super(props);

    this.state = {
      withDrawalList: []
    }

  }

  render () {
    const { url } = this.props
    const { withDrawalList } = this.state

    const myWithDrawalsState = (withDrawalList && withDrawalList.length > 0) ?
    (
      <div>
        <div className="grid-x grid-padding-x goals-slider">
          {
            withDrawalList.map((data, index) => {
              return (
                <div className="cell medium-12 large-6" key={index}>
                  <WithDrawalCards item={data}/>
                </div>
              )
            })
          }
        </div>
      </div>
    ) :
    (
      <div className="grid-x align-top align-center">
        <div className="cell small-12 medium-8 large-6 text-align-center">
          <p className="spacer">
            You have no withdrawals at this time.
          </p>
        </div>
      </div>
    )

    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-top align-center start-page">
          <div className="cell small-12 medium-12 large-10">
            <form name="profile">   
              <div className="grid-x grid-padding-x">
                <div className="cell small-12 medium-4 text-align-center">
                  <ProfileMenu/>
                </div>
                <div className="cell small-12 medium-8">
                  <div className="card white profile-data">
                    <div className="grid-x grid-padding-x">
                      <div className="cell small-8">
                        <h1 className="text-align-left purple-text">
                          <img src={require('../../../assets/img/withdrawals-icon.png')} height="20" style={{height: '25px'}}/> My Withdrawals
                        </h1>
                      </div>
                    </div>
                    {myWithDrawalsState}
                  </div>
                </div>   
              </div>
            </form>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}