import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'
import ProfileMenu from '../../../components/partials/profile-menu'
import Modal from '../../../components/partials/modal'
import AddBank from '../../../components/teams/add-bank'

export default class MyBanksPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cards: [{
        firstName: "Emeka",
        middleName: "Pius",
        lastName: "Timothy",
        bankName: "Guarantee Trust Bank",
        bankNo: "0123456789",
        bankIcon: "0123456789"
      },
      {
        firstName: "Timothy",
        middleName: "Emeka",
        lastName: "Pius",
        bankName: "Diamond Bank",
        bankNo: "0123456789",
        bankIcon: "0123456789"
      }],
      showModal: false
    };

    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  openModal(e) {
    e.preventDefault()
    this.setState({ showModal: true })
  }

  closeModal (e) {
    e.preventDefault()
    this.setState({ showModal: false })
  }

  addBankService (e) {
    e.preventDefault()
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState((state) => {
      let updatedState = state
      updatedState[name] = value
      return updatedState
    })
  }

  render () {
    const { url } = this.props
    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-top align-center start-page">
          <div className="cell small-12 medium-12 large-10 text-align-center">
            <form name="profile">   
              <div className="grid-x grid-padding-x">
                <div className="cell small-12 medium-4">
                  <ProfileMenu/>
                </div>
                <div className="cell small-12 medium-8">
                  <div className="card white profile-data">
                    <div className="grid-x align-top align-center">
                      <div className="cell small-12 ">
                      <h1 className="text-align-left purple-text">My Banks</h1>
                      <br/>
                      <br/>
                    </div>
                      <div className="cell small-12 medium-8 large-6 text-align-center">
                        <img src={require('../../../assets/img/add-bank-start.png')} className="start-icon" height="240"/>
                        <p className="spacer">
                          You have no banks saved. Add your banks to withdraw your personal savings. 
                        </p>
                        <button className="button purple expanded max-340" onClick={(e) => this.openModal(e)}>
                          ADD BANK
                        </button>
                      </div>
                    </div>
                  </div>
                </div>   
              </div>
            </form>
          </div>
        </div>
        <Modal showModal={ this.state.showModal } closeModal={this.closeModal}>
          <a className="close" onClick={(e) => this.closeModal(e)}/>
          <form>
            <AddBank {...this.props} handleInputChange={this.handleInputChange} callBack={this.addBankService}/>
          </form>
        </Modal>
      </DefaultLayout>
    )
  }
}