import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'
import ProfileMenu from '../../../components/partials/profile-menu'


export default class ProfilePage extends Component {
  render () {
    const { url } = this.props

    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-top align-center start-page">
          <div className="cell small-12 medium-12 large-10 text-align-center">
            <form name="profile">   
              <div className="grid-x grid-padding-x">
                <div className="cell small-12 medium-4">
                  <ProfileMenu/>
                </div>
                <div className="cell small-12 medium-8">
                  <div className="card white profile-data">
                    <div className="grid-x grid-padding-x">
                      <div className="cell small-12 ">
                        <h1 className="text-align-left purple-text">Edit Your Profile</h1>
                        <br/>
                        <br/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <label className="text-align-left">First Name</label>
                        <input type="text" name="firstName"/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <label className="text-align-left">Last Name</label>
                        <input type="text" name="lastName"/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <label className="text-align-left">Email</label>
                        <input type="text" name="email"/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <label className="text-align-left">Phone</label>
                        <input type="text" name="phone"/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <label className="text-align-left">Password</label>
                        <input type="password" name="password"/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <label className="text-align-left">Re enter Password</label>
                        <input type="password" name="address"/>
                      </div>
                      <div className="cell small-12 medium-6">
                        <button className="button purple expanded">
                          SAVE CHANGES
                        </button>
                      </div>
                    </div>
                  </div>
                </div>   
              </div>
            </form>
          </div>
        </div>
      </DefaultLayout>
    )
  }
}
