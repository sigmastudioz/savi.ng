import React, { Component } from 'react'
import DefaultLayout from '../../../layouts/default'
import ProfileMenu from '../../../components/partials/profile-menu'
import Modal from '../../../components/partials/modal'
import AddCard from '../../../components/teams/add-card'

export default class MyCardsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // cards: [{
      //   firstName: "Emeka",
      //   middleName: "Pius",
      //   lastName: "Timothy",
      //   lastNo: "3745",
      //   month: "03",
      //   year: "22"
      // },
      // {
      //   firstName: "Timothy",
      //   middleName: "Emeka",
      //   lastName: "Pius",
      //   lastNo: "3745",
      //   month: "03",
      //   year: "22"
      // }],
      cards: [],
      showModal: false
    };

    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }


  openModal(e) {
    e.preventDefault()
    this.setState({ showModal: true })
  }

  closeModal (e) {
    e.preventDefault()
    this.setState({ showModal: false })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState((state) => {
      let updatedState = state
      updatedState[name] = value
      return updatedState
    })
  }

  addDebitCardService (e) {
    e.preventDefault()
  }

  render () {
    const { url } = this.props
    const CardList = this.state.cards.map((card, index) => {
      return (
        <div className="cell medium-12 large-6" key={index}>
          <div className="card debit-card alternate text-align-left">
            <div className="grid-x grid-padding-x align-bottom">
              <div className="cell small-10">
                <div className="grid-x grid-padding-x">
                  <div className="cell auto">****</div>
                  <div className="cell auto">****</div>
                  <div className="cell auto">****</div>
                  <div className="cell auto">{card.lastNo}</div>
                </div>
              </div>
              <div className="cell small-6">
                <span className="info">{card.firstName} {card.lastName}</span>
                <span className="info-label">Card Holder</span>
              </div>
              <div className="cell small-6">
              <span className="info">{card.month}/{card.year}</span>
              <span className="info-label">Exp. Date</span>
            </div>
            </div>
          </div>
        </div>
      )
    })

    return (
      <DefaultLayout url={url}>
        <div className="grid-x align-top align-center start-page">
          <div className="cell small-12 medium-12 large-10 text-align-center">
            <form name="profile">   
              <div className="grid-x grid-padding-x">
                <div className="cell small-12 medium-4">
                  <ProfileMenu/>
                </div>
                <div className="cell small-12 medium-8">
                  <div className="card white profile-data">
                    <div className="cell small-12 ">
                      <h1 className="text-align-left purple-text">My Cards</h1>
                      <br/>
                      <br/>
                    </div>
                    <div className={`grid-x ${(this.state.cards.length === 0) ? 'align-middle align-center-middle' : 'align-top align-center grid-padding-x'}`}>
                      {
                        (this.state.cards.length === 0) ? (
                          <div className="cell small-12 medium-8 large-6 text-align-center">
                            <img src={require('../../../assets/img/add-card-start.png')} className="start-icon" height="240"/>
                            <p className="spacer">
                              There are no debit cards saved. Add multiple cards to save effortlessly.
                            </p>
                            <button className="button purple expanded max-340" onClick={(e) => this.openModal(e)}>
                              ADD CARD
                            </button>
                          </div>
                        ) : CardList
                      }
                    </div>
                  </div>
                </div>   
              </div>
            </form>
          </div>
        </div>
        <Modal showModal={ this.state.showModal } closeModal={this.closeModal}>
          <a className="close" onClick={(e) => this.closeModal(e)}/>
          <form>
            <AddCard {...this.props} handleInputChange={this.handleInputChange}
            button={(
              <div className="cell small-12">
                <button className="button purple expanded max-340" onClick={(e) => this.addDebitCardService(e)}>
                  SAVE
                </button>
              </div>
            )}/>
          </form>
        </Modal>
      </DefaultLayout>
    )
  }
}