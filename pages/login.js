import React, { Component } from 'react'
import Link from 'next/link'
import LandingPage from '../layouts/landing'

class Login extends Component {
    render(){
        return (
                <div className="grid-x align-middle align-center-middle start-page">
                    <div className="cell small-12 medium-6">
                        <div className="grid-x grid-padding-x align-bottom"
                            style={{
                                backgroundImage: `linear-gradient(to top, #5f72bd 0%, #9b23ea 100%)`,
                                backgroundSize: "cover",
                                backgroundPosition: "center",
                                minHeight: "100vh"
                            }}>
                        <div className="cell small-12">
                            <h5 style={{color: '#ffffff'}} className="subheader">
                                Savi.ng provides the easiest way to save money 
                                while earning the highest interest rates in Nigeria
                            </h5>
                        </div>
                    </div>
                    </div>
                    <div className="cell small-12 medium-6 text-align-center" style={{padding : '50px'}}>
                        <div className="card white">
                                <div>
                                    <img src={require('../assets/img/saviLogo.svg')}  height="40"/><span className="badge badge-pill badge-danger">Beta</span>
                                </div>
                                <div>
                                    <form name="login">
                                        <p className="spacer"> Login to Continue </p>
                                        <input type="text" placeholder="Email"/>
                                        <input type="text" placeholder="Password"/>
                                        <button className="button purple expanded"> SIGN IN </button>
                                    </form> 
                                </div>
                                <h6 className="spacer" style={{paddingTop: '15px'}}>
                                    <small>&copy; 2018 VFD Micro Finance Bank All rights reserved</small>
                                </h6>
                        </div>
                    </div>
                </div>
        )
    }
}

export default Login