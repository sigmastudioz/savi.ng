import React, { Component } from 'react'
import Link from 'next/link'
import moment from 'moment'
import fetch from 'node-fetch'

import DefaultLayout from '../layouts/default'
import Stepper from '../components/teams/stepper'
import Footer from '../components/partials/footer'
import { GoalDetails } from '../components/goals/saving-details' 
import AddCard from '../components/teams/add-card'
import Modal from '../components/partials/modal'

const YourDetails = ({ handleInputChange, nextStep, inputStates, title, description, onFocus, onBlur }) => {
  return (
    <div className="grid-x align-top align-center">
      <div className="cell small-12 medium-10">
        <div className="grid-x grid-padding-x">
          <div className="cell small-12">
            { title }
            { description }
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.first_name ? 'active' : ''}`}>First Name</label>
            <input type="text" name="first_name" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.last_name ? 'active' : ''}`}>Last Name</label>
            <input type="text" name="last_name" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.gender ? 'active' : ''}`}>Sex</label>
            <select
              name="gender" 
              onChange={handleInputChange}>
              <option value="F">Female</option>
              <option value="M">Male</option>
            </select>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.dob ? 'active' : ''}`}>Date of birth</label>
            <input type="date" name="dob" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.email ? 'active' : ''}`}>Email</label>
            <input type="text" name="email" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.email ? 'active' : ''}`}>Cell Phone Number</label>
            <input type="text" name="email" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.password ? 'active' : ''}`}>Password</label>
            <input type="password" name="password" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12 medium-6">
            <label className={`text-align-left ${inputStates && inputStates.password_confirm ? 'active' : ''}`}>Password</label>
            <input type="password" name="password_confirm" 
              onKeyUp={handleInputChange}
              onFocus={onFocus} 
              onBlur={onBlur}/>
          </div>
          <div className="cell small-12">
            <button className="button purple expanded" onClick={(e) => nextStep(e, 1)}>
              CONTINUE
            </button>
          </div>
          <div className="cell small-12 text-align-center small-text">
            <p>Already have an Account? <Link prefetch href={{ pathname: '/login' }}><a>Login</a></Link></p>
          </div>
        </div>
      </div>
    </div>
  )
}

const VerifyPhone = ({ handleInputChange, resendVerification, title, description, nextStep }) => {
  return (
    <div className="grid-x align-top align-center">
      <div className="cell small-12 medium-10">
        <div className="grid-x grid-padding-x">
          <div className="cell small-12">
            {title}
            {description}
          </div>
        </div>
        <div className="grid-x grid-padding-x verify-numbers">
          <div className="cell auto">
            <input type="text" name="verify_1" onKeyUp={handleInputChange} maxLength="1" minLength="1" />
          </div>
          <div className="cell auto">
            <input type="text" name="verify_2" onKeyUp={handleInputChange} maxLength="1" minLength="1" />
          </div>
          <div className="cell auto">
            <input type="text" name="verify_3" onKeyUp={handleInputChange} maxLength="1" minLength="1" />
          </div>
          <div className="cell auto">
            <input type="text" name="verify_4" onKeyUp={handleInputChange} maxLength="1" minLength="1" />
          </div>
          <div className="cell auto">
            <input type="text" name="verify_5" onKeyUp={handleInputChange} maxLength="1" minLength="1" />
          </div>
          <div className="cell auto">
            <input type="text" name="verify_6" onKeyUp={handleInputChange} maxLength="1" minLength="1" />
          </div>
          <div className="cell small-12">
            <button className="button purple expanded" onClick={(e) => nextStep(e, 2)}>
              VERIFY
            </button>
          </div>
          <div className="cell small-12 text-align-center small-text">
            <p>Didn't get the code? <a  onClick={(e) => resendVerification()}>Resend</a></p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default class IndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focusedInput: 'startDate',
      first_name: null,
      last_name: null,
      email: null,
      phone_number: null,
      password: null,
      password_confirm: null,
      verify_1: null,
      verify_2: null,
      verify_3: null,
      verify_4: null,
      verify_5: null,
      verify_6: null,
      currentStep: 0,
      startDate: moment(),
      debit_card: "",
      frequency: 'days',
      saving_goal: null,
      periodic_amount: 1,
      periodic_amount_format: 1,
      periodic_amount_extra: 1,
      periodic_amount_extra_format: 1,
      endDate: moment(),
      processed: false,
      card_number: null,
      expire_date: moment(),
      cvc: null,
      showLoading: false,
      inputStates: {},
      ...props
    }

    this.nextStep = this.nextStep.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValueChange = this.handleValueChange.bind(this)
    this.onFocusChange = this.onFocusChange.bind(this)
    this.onDatesChange = this.onDatesChange.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.onBlur = this.onBlur.bind(this)
  }

  nextStep(e, stepIndex) {
    e.preventDefault()
    if (!this.state.processed) {
      this.setState({
        currentStep: stepIndex
      })
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState[name] = value
      return updatedState;
    })
  }

  handleValueChange (values, property, property_format) {
    const {formattedValue, value} = values
    var update = {}
    update[property] = value
    update[property_format] = formattedValue
    this.setState(update)
  }

  onFocus(event) {
    const target = event.target;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = true
      return updatedState;
    })
  }

  onBlur(event) {
    const target = event.target;
    const name = target.name;

    this.setState((state) => {
      let updatedState = state
      updatedState.inputStates[name] = false
      return updatedState;
    })
  }

  onFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  onDatesChange({ startDate, endDate }) {
    this.setState({ startDate, endDate })
  }

  closeModal (e) {
    e.preventDefault()
    this.setState({ showModal: false });
  }

  registerUser (e) {
    e.preventDefault()

    const { first_name, last_name, password, email, gender, dob, phone_number } = this.state
    fetch('https://api.savi.ng/signup/', { 
      method: 'post',
      body: JSON.stringify({
        first_name,
        last_name,
        password,
        email,
        profile: {
          gender,
          dob,
          phone_number,
          pay_roll: 'NS'
        }
      }),
      headers: { 
        'Access-Control-Allow-Origin': true,
        'Content-Type': 'application/json' 
      }
    }).then((data) => {
      console.log(data)
      if (data.status) {
        // this.setState({
        //   processed: true
        // })
      } else {
        // show Error modal, show data.meessages
      }
    })
    .catch((e) => {
      // show error Modal. Show server response
    })
  }

  render () {
    const { url, currentStep, frequency, startDate, endDate, saving_goal, periodic_amount, periodic_amount_extra, debit_card, processed, showLoading } = this.state

    return (
      <DefaultLayout url={url} hideNav={true} className="stretch-page">
        <div className="grid-x align-middle">
          <div className="cell small-12 sign-up-header text-center">
            <Link href="/">
              <a>
                <img src={require('../assets/img/saviLogo.svg')} height="30"/>
                <span className="badge badge-pill badge-danger">Beta</span>
              </a>
            </Link>
          </div>
        </div>
        <div className="grid-x align-middle align-center-middle">
          <div className="cell small-12 text-align-center">
            <form name="signup">
              <Stepper
                currentStep={currentStep} nextStep={this.nextStep} showCardBg={false}>
                <YourDetails {...this.state} 
                  title={(
                    <h2>
                      The smarter way to grow your Naira
                    </h2>
                  )}
                  description={(
                    <p className="summary">
                      Psst!...CBN requires we ask these questions
                    </p>
                  )}
                  name="Your Details"
                  handleInputChange={this.handleInputChange} 
                  onFocus={this.onFocus} 
                  onBlur={this.onBlur} 
                  nextStep={this.nextStep}/>
                <VerifyPhone {...this.state} 
                  title={(
                    <h2>
                      One step verification
                    </h2>
                  )}
                  description={(
                    <p className="summary">
                      Enter the code we sent to your phone number
                    </p>
                  )}
                  name="Verify Phone"
                  resendVerification={this.resendVerification}
                  handleInputChange={this.handleInputChange} 
                  onFocus={this.onFocus}
                  onBlur={this.onBlur}
                  nextStep={this.nextStep}/>
                <GoalDetails {...this.state} 
                  title={(
                    <h2>
                      Your first goal is a click away.
                    </h2>
                  )}
                  description={(
                    <p className="summary">
                      Customize your saving habit. Fill in the details below. 
                    </p>
                  )}
                  handleValueChange={this.handleValueChange}
                  handleInputChange={this.handleInputChange} 
                  nextStep={this.nextStep}
                  name="Create New Goal"
                  onFocusChange={this.onFocusChange} 
                  onDatesChange={this.onDatesChange}
                  onFocus={this.onFocus}
                  onBlur={this.onBlur}
                  button={(
                    <div className="cell small-12">
                      <button className="button purple expanded" onClick={(e) => this.nextStep(e, 3)}>
                        CONTINUE
                      </button>
                    </div>
                  )}/>
                <div className="grid-x align-top align-center" name="Get Started">
                  <div className="cell small-12 medium-10">
                  <AddCard  
                    {...this} 
                    title={(
                      <h2>
                        Enter card details to begin
                      </h2>
                    )}
                    description={(
                      <p className="summary">
                        No worries, your debit card will be saved securely.
                      </p>
                    )}
                    handleInputChange={this.handleInputChange}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    button={(
                      <div className="cell small-12">
                        <button className="button purple expanded" onClick={(e) => this.registerUser(e)}>
                          FINISH
                        </button>
                      </div>
                    )}/>
                  </div>
                </div>
                
              </Stepper>
            </form>
          </div>
        </div>
        <Modal showModal={ this.state.currentStep === 4 }>
          <form name="add-funds">
            <div className="grid-x align-middle align-center-middle">
              <div className="cell small-12 medium-10 text-align-center">
              <h2>Goal created successfully</h2>
              <div style={{padding: '40px 0'}}><img src={require('../assets/img/verify-icon.png')} style={{height: '125px'}}/></div>
              <p className="spacer">
                No worries, your debit card will be saved securely.
              </p>
              <Link prefetch href={{ pathname: '/account/savings'}}>
                <a className={`button purple expanded max-340`}>
                  PROCEED TO DASHBOARD
                </a>
              </Link>
            </div>
            </div>
          </form>
        </Modal>
        <Footer/>
      </DefaultLayout>
    )
  }
}