import React, { Component } from 'react'
import LandingPageLayout from '../layouts/landing'
import Link from 'next/link'
import LandingPageHeader from '../components/partials/LandingPageHeader'
import Footer from '../components/partials/footer' 

export default class IndexPage extends Component {
  render () {
    const { url } = this.props
    return (
      <div>
          <LandingPageLayout url={url} hideNav={true} className="home-page">
            <div className="main-banner">
            <LandingPageHeader style={{ backgroundColor: '#FAF5FF'}}></LandingPageHeader>
              <div className="grid-x align-middle align-center-middle home-splash">
                <div className="cell small-12 medium-6">
                  <h3>Take your first step to financial freedom</h3>
                  <p className="w70p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempus ultrices hendrerit. 
                  </p>
                  <div className="grid-x app-stores">
                    <div className="cell small-12 medium-3">
                      <a href="#">
                        <img src={require('../assets/img/badge-ios.png')} />
                      </a>
                    </div>
                    <div className="cell small-12 medium-3">
                      <a href="#">
                        <img src={require('../assets/img/badge-android.png')} />
                      </a>
                    </div>
                  </div>
                </div>
                <div className="cell small-12 medium-6 text-align-center">
                  <a href="#">
                    <img src={require('../assets/img/homepage-mobile-phone-overlay.png')} />
                  </a>
                </div>
              </div>
            </div>
            <div className="home-getting-started">
              <div className="grid-x align-middle align-center-middle home-splash">
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-middle">
                    <div className="cell small-12 medium-6">
                      <h3>Getting Started</h3>
                      <p>
                        Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  nam.
                    </p>
                    </div>
                  </div>
                </div>
                <div className="cell small-12 medium-6">
                  <h3>Get Rewarded For Saving</h3>
                  <p className="w70p">
                    Lorem ipsum dolor sit amet, populo impedit nec
                    ex, cu sit principes consequat, ei primis nemore
                    nam. Sed et decore consul adolescens. Vel
                    eripuit dissentiet ut, an his aperiam insolens. cu
                    sit principes consequat, ei primis.
                </p>
                </div>
                <div className="cell small-12 medium-6 text-align-center">
                  <img src={require('../assets/img/homepage-vault.png')} />
                </div>
              </div>
              <div className="grid-x align-middle align-center-middle home-splash">
                <div className="cell small-12 medium-6 text-align-center">
                  <img src={require('../assets/img/homepage-savings.png')} />
                </div>
                <div className="cell small-12 medium-6">
                  <h3>Create Savings Account</h3>
                  <p className="w70p">
                    Lorem ipsum dolor sit amet, populo impedit nec
                    ex, cu sit principes consequat, ei primis nemore
                    nam. Sed et decore consul adolescens. Vel
                    eripuit dissentiet ut, an his aperiam insolens. cu
                    sit principes consequat, ei primis.
                </p>
                </div>
              </div>
              <div className="grid-x align-middle align-center-middle home-splash">
                <div className="cell small-12 medium-6">
                  <h3>Set Up Your Debit Card</h3>
                  <p className="w70p">
                    Lorem ipsum dolor sit amet, populo impedit nec
                    ex, cu sit principes consequat, ei primis nemore
                    nam. Sed et decore consul adolescens. Vel
                    eripuit dissentiet ut, an his aperiam insolens. cu
                    sit principes consequat, ei primis.
                </p>
                </div>
                <div className="cell small-12 medium-6 text-align-center">
                  <img src={require('../assets/img/homepage-debit-card.png')} />
                </div>
              </div>
              <div className="grid-x align-middle align-center-middle home-splash">
                <div className="cell small-12 medium-6 text-align-center">
                  <img src={require('../assets/img/homepage-automatic-save.png')} />
                </div>
                <div className="cell small-12 medium-6">
                  <h3>Start Saving Automatically</h3>
                  <p className="w70p">
                    Lorem ipsum dolor sit amet, populo impedit nec
                    ex, cu sit principes consequat, ei primis nemore
                    nam. Sed et decore consul adolescens. Vel
                    eripuit dissentiet ut, an his aperiam insolens. cu
                    sit principes consequat, ei primis.
              </p>
                </div>
              </div>
            </div>
            <div className="home-splash-expanded purple-bg">
              <div className="grid-x align-middle align-center-middle home-splash">
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-middle">
                    <div className="cell small-12 medium-6">
                      <h3>Companies That Trust us</h3>
                      <p>
                        Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  nam.
                    </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="grid-x align-middle align-center-middle home-splash visa-splash">
                <div className="cell small-12 medium-6">
                  <h3>Start Saving Automatically</h3>
                  <p className="w70p">
                    Lorem ipsum dolor sit amet, populo impedit nec
                    ex, cu sit principes consequat, ei primis nemore
                    nam. Sed et decore consul adolescens. Vel
                    eripuit dissentiet ut, an his aperiam insolens. cu
                    sit principes consequat, ei primis.
                </p>
                </div>
                <div className="cell small-12 medium-6 text-align-center">
                  <img src={require('../assets/img/homepage-visa.png')} />
                </div>
              </div>
            </div>
            <div className="grid-x home-splash shadow-box">
              <div className="cell small-12 text-center">
                <h3>What people are saying</h3>
              </div>
              <div className="cell small-12 medium-6 text-align-center">

              </div>
              <div className="cell small-12 medium-6 text-align-center">
                <blockquote>
                  Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes conseq, ei primis nemore  nam. Sed et decore consul adolescens.
              </blockquote>
              </div>
            </div>
            <Footer/>
          </LandingPageLayout>
          
      </div>
      
    )
  }
}