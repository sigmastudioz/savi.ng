import React from 'react'
import App, { Container } from 'next/app'
// import withNProgress from 'next-nprogress'
import Router from 'next/router'
import { Provider } from 'react-redux'
import withRedux from 'next-redux-wrapper'
import withReduxSaga from 'next-redux-saga'
import { PageTransition } from 'next-page-transitions'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'

import createStore from '../stores'

const TIMEOUT = 400
library.add(faBars)
library.add(faTimes)

class SavingNG extends App {
  static async getInitialProps ({ Component, router, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render () {
    const { Component, pageProps, store } = this.props
    return (
      <Container>
        <Component {...pageProps} />
      </Container>
    )
  }
}
// <Container>
// <Provider store={store}>
//   <PageTransition
//     timeout={TIMEOUT}
//     classNames='page-transition'
//     loadingDelay={500}
//     loadingTimeout={{
//       enter: TIMEOUT,
//       exit: 0
//     }}
//     loadingClassNames='loading-indicator'
//   >
//     <Component {...pageProps} />
//   </PageTransition>
// </Provider>
// </Container>

const msDelay = 1000; // default is 300
//, withNProgress(msDelay)
// export default withRedux(createStore)(withReduxSaga({ async: true })(SavingNG))
export default SavingNG
