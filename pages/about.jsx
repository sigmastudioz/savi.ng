import React, { Component } from 'react'
import LandingPageLayout from '../layouts/landing'
import Link from 'next/link'
import LandingPageHeader from '../components/partials/LandingPageHeader'
import Footer from '../components/partials/footer' 

export default class AboutPage extends Component {
  render () {
    const { url } = this.props

    const teams = [
      {
        firstName: "Thomas",
        lastName: "Doe",
        jobTitle: ""
      },
      {
        firstName: "Jane",
        lastName: "Doe",
        jobTitle: "Front End Dev"
      },
      {
        firstName: "Tom",
        lastName: "Doe",
        jobTitle: "Product Designer"
      },
      {
        firstName: "John",
        lastName: "Doe",
        jobTitle: "UI/UX"
      },
      {
        firstName: "Mary",
        lastName: "Doe",
        jobTitle: "Back End Dev"
      },
      {
        firstName: "Harry",
        lastName: "Doe",
        jobTitle: "Business"
      }
    ]

    return (
      <div>
          <LandingPageLayout url={url} hideNav={true} className="home-page">
            <div className="main-banner">
            <LandingPageHeader style={{ backgroundColor: '#FAF5FF'}}/>
              <div className="grid-x align-middle align-center-middle sub-page-splash">
                <div className="cell small-12">
                  <h2>About Us</h2>
                </div>
              </div>
              <div className="grid-x align-middle align-center-middle sub-page-splash-alt">
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-middle">
                    <div className="cell small-12 medium-6">
                      <p>
                        Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  nam. 
                        Sed et decore consul adolescens. Vel eripuit dissentiet ut, an his aperiam insolens. cu sit principes 
                        consequat, ei primis. Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, 
                        ei primis nemore  nam. Sed et decore consul adolescens. 
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="grid-x align-middle align-center-middle sub-page-splash-alt-2">
                <h2 className="cell small-12 text-center">
                  Our Solution
                </h2>
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-top">
                    <div className="cell small-12 medium-6">
                      <p className="text-align-left">
                        Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  
                        nam. Sed et decore consul adolescens. Vel eripuit dissentiet ut, an his aperiam insolens. cu sit 
                        principes consequat, ei primis. Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes 
                        consequat, ei primis nemore  nam. Sed et decore consul adolescens. Vel eripuit dissentiet ut, an his aperiam.
                        <br/>
                        - PATRICK DOE, CEO <br/>
                        Savi.ng 
                      </p>
                    </div>
                    <div className="cell small-12 medium-6 text-center">
                      <div className="bubble">
                      
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="grid-x align-middle align-center-middle sub-page-splash-alt">
                <h2 className="cell small-12 text-center">
                  Our Team
                </h2>
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-top team-members">
                    {
                      teams.map((member, index) => {
                        return (
                          <div className="cell small-12 medium-4 text-center member" key={index}>
                            <div className="avatar">
                            
                            </div>
                            <h3>{member.firstName} {member.lastName}</h3>
                            <h4>{member.jobTitle}</h4>
                          </div>
                        )
                      })
                    }
                  </div>
                </div>
              </div>

              <div className="grid-x align-middle align-center-middle sub-page-splash-alt-2">
                <h2 className="cell small-12 text-center">
                  Career Opportunities
                </h2>
                <div className="cell small-12 text-align-center">
                  <div className="grid-x align-center-middle">
                    <div className="cell small-12 medium-6">
                      <p className="text-align-center">
                        Lorem ipsum dolor sit amet, populo impedit nec ex, cu sit principes consequat, ei primis nemore  
                        nam. Sed et decore consul adolescens. Vel eripuit dissentiet ut, an his aperiam insolens. cu sit 
                        principes consequat, ei primis.
                      </p>
                      <br/>
                      <Link prefetch href={{ pathname: '/contact-us' }}>
                        <a className="button purple">
                          EMAIL US
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer/>
          </LandingPageLayout>
          
      </div>
      
    )
  }
}