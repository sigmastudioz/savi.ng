const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()


// New call to compress content
// app.use("/", express.static(path.join(__dirname, '_next')));

app.prepare()
  .then(() => {
    const server = express()

    server.get('/p/:id', (req, res) => {
      const actualPage = '/post'
      const queryParams = { title: req.params.id } 
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/saving', (req, res) => {
      const actualPage = '/account/savings'
      const queryParams = { title: req.params.id } 
      app.render(req, res, actualPage, queryParams)
    })
    
    server.get('/saving/:details', (req, res) => {
      const actualPage = '/account/savings/details'
      const queryParams = { title: req.params.id } 
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/teams', (req, res) => {
      const actualPage = '/account/teams'
      const queryParams = { title: req.params.id } 
      app.render(req, res, actualPage, queryParams)
    })
    
    server.get('/teams/:details', (req, res) => {
      const actualPage = '/account/teams/details'
      const queryParams = { title: req.params.id } 
      app.render(req, res, actualPage, queryParams)
    })
  

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
